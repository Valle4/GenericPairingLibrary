#ifndef GENERICPAIRING_H
#define GENERICPAIRING_H


/**
 * @file genericpairing.h
 * @brief Header of the library providing the IPC interface for the generic pairing daemon
 *
 * It contains
 *   - default values used by both the pairing daemon and the front-end
 *   - the enum definition for all IPC messages
 *   - convenience functions for direct use in a client application
 */


#include "genericpairing_global.h"

#include <string>


/**
 * @namespace GenericPairing
 * @brief Namespace of the generic pairing library
 */
namespace GenericPairing {

    // Shared string values

    /**
     * @brief Name of the unix domain socket used for IPC
     */
    extern GENERICPAIRINGSHARED_EXPORT std::string IpcServerName;
    /**
     * @brief Service type used for mDNS-SD discovery
     */
    extern GENERICPAIRINGSHARED_EXPORT std::string MdnssdServiceType;
    /**
     * @brief Default path to save pairings in
     */
    extern GENERICPAIRINGSHARED_EXPORT std::string DefaultPairingPath;
    /**
     * @brief Default port for pairing requests over network
     */
    extern GENERICPAIRINGSHARED_EXPORT u_int16_t DefaultPort;
    /**
     * @brief Default port for IPC when unix domain sockets are unavailable
     */
    extern GENERICPAIRINGSHARED_EXPORT u_int16_t DefaultLoopbackIpcPort; // Only needed for Android
    /**
     * @brief Default length of the shared secret, in bytes
     */
    extern GENERICPAIRINGSHARED_EXPORT size_t DefaultSharedSecretByteLength;
    /**
     * @brief Default length of the SAS (Short Authentication String), in bytes
     */
    extern GENERICPAIRINGSHARED_EXPORT size_t DefaultSasByteLength; // Short Authentication String
    /**
     * @brief Default listen time (ms) for pairing requests over network
     */
    extern GENERICPAIRINGSHARED_EXPORT size_t DefaultListenTime;
    /**
     * @brief Default wait time (ms) for connects etc.
     */
    extern GENERICPAIRINGSHARED_EXPORT size_t DefaultWaitTime;


    typedef u_int16_t message_t;
    /**
     * @enum IpcMessage
     * @brief Messages for IPC
     */
    enum class GENERICPAIRINGSHARED_EXPORT IpcMessage : message_t {
        Request_Decryption, // + ciphertext
        Request_Signature, // + message
        Request_GetPairing, // + contact name
        Request_GetPublicKey, // + contact name
        Request_GetSharedSecret, // + contact name
        Request_WaitForPairingRequest, // + contact name
        Request_SendPairingRequest, // + contact name + contact IP + contact port
        Request_RemovePairing, // + contact name
        Success,
        Failure
    };



    // Functions related to static private key

    /**
     * @brief Convenience function for decryption with the static private key
     *
     * It handles the necessary IPC by sending the Decryption request
     * to the local pairing daemon and blocking until the answer arrives.
     *
     * @param ciphertext to decrypt
     * @param plaintext to hold resulting plaintext
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    GENERICPAIRINGSHARED_EXPORT bool decrypt(const std::string& ciphertext, std::string* plaintext, std::string* error = nullptr);

    /**
     * @brief Convenience function for signing with the static private key
     *
     * It handles the necessary IPC by sending the Signature request
     * to the local pairing daemon and blocking until the answer arrives.
     *
     * @param message to sign
     * @param signature to hold resulting signature
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    GENERICPAIRINGSHARED_EXPORT bool sign(const std::string& message, std::string* signature, std::string* error = nullptr);


    // Functions related to existing pairings

    /**
     * @brief Convenience function for getting the pairing data corresponding to a given contact
     *
     * It handles the necessary IPC by sending the GetPairing request
     * to the local pairing daemon and blocking until the answer arrives.
     *
     * @param contactName name of contact
     * @param pairing to hold pairing data
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    GENERICPAIRINGSHARED_EXPORT bool getPairing(const std::string& contactName, std::string* pairing, std::string* error = nullptr);

    /**
     * @brief Convenience function for getting the public key of a given contact
     *
     * It handles the necessary IPC by sending the GetPublicKey request
     * to the local pairing daemon and blocking until the answer arrives.
     *
     * @param contactName name of contact
     * @param publicKey to hold public key
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    GENERICPAIRINGSHARED_EXPORT bool getPublicKey(const std::string& contactName, std::string* publicKey, std::string* error = nullptr);

    /**
     * @brief Convenience function for getting the shared secret corresponding to a given contact
     *
     * It handles the necessary IPC by sending the GetSharedSecret request
     * to the local pairing daemon and blocking until the answer arrives.
     *
     * @param contactName name of contact
     * @param sharedSecret to hold shared secret
     * @param error to hold errors
     * @return true if successful, false otherwise
     */
    GENERICPAIRINGSHARED_EXPORT bool getSharedSecret(const std::string& contactName, std::string* sharedSecret, std::string* error = nullptr);
}

#endif // GENERICPAIRING_H
