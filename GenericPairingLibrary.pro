#-------------------------------------------------
#
# Project created by QtCreator 2016-06-19T18:46:50
#
#-------------------------------------------------

QT       += core network

QT       -= gui

TARGET = genericpairing
CONFIG += c++11

TEMPLATE = lib

DEFINES += GENERICPAIRING_LIBRARY

SOURCES += \
    genericpairing.cpp

HEADERS +=\
    genericpairing.h \
    genericpairing_global.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
