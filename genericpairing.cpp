#include "genericpairing.h"

#include <QtNetwork>



GENERICPAIRINGSHARED_EXPORT std::string GenericPairing::IpcServerName = "pairingd.localsocket";
GENERICPAIRINGSHARED_EXPORT std::string GenericPairing::MdnssdServiceType = "_pairing._tcp";
GENERICPAIRINGSHARED_EXPORT std::string GenericPairing::DefaultPairingPath = "./Pairings/";
GENERICPAIRINGSHARED_EXPORT u_int16_t GenericPairing::DefaultPort = 1337;
GENERICPAIRINGSHARED_EXPORT u_int16_t GenericPairing::DefaultLoopbackIpcPort = 1338;
GENERICPAIRINGSHARED_EXPORT size_t GenericPairing::DefaultSharedSecretByteLength = 32; // 256Bit
GENERICPAIRINGSHARED_EXPORT size_t GenericPairing::DefaultSasByteLength = 2; // 16Bit
GENERICPAIRINGSHARED_EXPORT size_t GenericPairing::DefaultListenTime = 60000; // 60s
GENERICPAIRINGSHARED_EXPORT size_t GenericPairing::DefaultWaitTime = 4000; // 4s



bool sendRequestAndGetAnswer(GenericPairing::IpcMessage request, const std::string& text, std::string* result, std::string* error){
#ifdef Q_OS_ANDROID
    QTcpSocket socket;
    socket.connectToHost("localhost",GenericPairing::DefaultPort);
#else
    QLocalSocket socket;
    socket.connectToServer(QString::fromStdString(GenericPairing::IpcServerName));
#endif // Q_OS_ANDROID
    socket.waitForConnected();

    QDataStream stream(&socket);
    stream << (GenericPairing::message_t) request << QString::fromStdString(text);
    while (!socket.canReadLine())
        socket.waitForReadyRead();

    GenericPairing::message_t answer_raw;
    QString answerText;
    stream >> answer_raw >> answerText;
    GenericPairing::IpcMessage answer = (GenericPairing::IpcMessage) answer_raw;

    switch (answer) {
        case GenericPairing::IpcMessage::Success :
            *result = answerText.toStdString();
            return true;
        case GenericPairing::IpcMessage::Failure :
            if (error != nullptr)
                *error = answerText.toStdString();
            return false;
        default:
            *error = "Got answer with unknown or unexpected message type";
            return false;
    }
}



GENERICPAIRINGSHARED_EXPORT bool GenericPairing::decrypt(const std::string& ciphertext, std::string* plaintext, std::string* error){
    return sendRequestAndGetAnswer(GenericPairing::IpcMessage::Request_Decryption, ciphertext, plaintext, error);
}

GENERICPAIRINGSHARED_EXPORT bool GenericPairing::sign(const std::string& message, std::string* signature, std::string* error){
    return sendRequestAndGetAnswer(GenericPairing::IpcMessage::Request_Signature, message, signature, error);
}



GENERICPAIRINGSHARED_EXPORT bool GenericPairing::getPairing(const std::string& contactName, std::string* pairing, std::string* error){
    return sendRequestAndGetAnswer(GenericPairing::IpcMessage::Request_GetPairing, contactName, pairing, error);
}

GENERICPAIRINGSHARED_EXPORT bool GenericPairing::getPublicKey(const std::string& contactName, std::string* publicKey, std::string* error){
    return sendRequestAndGetAnswer(GenericPairing::IpcMessage::Request_GetPublicKey, contactName, publicKey, error);
}

GENERICPAIRINGSHARED_EXPORT bool GenericPairing::getSharedSecret(const std::string& contactName, std::string* sharedSecret, std::string* error){
    return sendRequestAndGetAnswer(GenericPairing::IpcMessage::Request_GetSharedSecret, contactName, sharedSecret, error);
}
